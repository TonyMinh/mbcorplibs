package com.mbcorp.textfontslib;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Minh on 7/13/2016.
 */
public class TextViewCustom extends TextView {
    private TypedArray typedArray;

    public TextViewCustom(Context context) {
        super(context);
        setTypeface(Utils.customTextView(context, -1));
    }

    public TextViewCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
        try {
            typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TextViewCustom, 0, 0);
            setTypeface(Utils.customTextView(context, typedArray.getInteger(R.styleable.TextViewCustom_type, 0)));
        } finally {
            typedArray.recycle();
        }
    }

    public TextViewCustom(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        try {
            typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TextViewCustom, 0, 0);
            setTypeface(Utils.customTextView(context, typedArray.getInteger(R.styleable.TextViewCustom_type, 0)));
        } finally {
            typedArray.recycle();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TextViewCustom(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        try {
            typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TextViewCustom, 0, 0);
            setTypeface(Utils.customTextView(context, typedArray.getInteger(R.styleable.TextViewCustom_type, 0)));
        } finally {
            typedArray.recycle();
        }
    }

}
