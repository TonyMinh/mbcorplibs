package com.mbcorp.textfontslib;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Minh on 7/13/2016.
 */
public class Utils {
    public static Typeface customTextView(Context context, int type) {
        switch (type) {
            case 0:
                return setTextView(context, "BreeSerif-Regular");

            case 1:
                return setTextView(context, "Roboto-Bold");

            case 2:
                return setTextView(context, "Roboto-Italic");

            case 3:
                return setTextView(context, "Roboto-Light");

            case 4:
                return setTextView(context, "Roboto-LightItalic");

            case 5:
                return setTextView(context, "Roboto-Medium");

            case 6:
                return setTextView(context, "Roboto-Regular");

            case 7:
                return setTextView(context, "Roboto-Thin");

            case 8:
                return setTextView(context, "Roboto-ThinItalic");

            default:
                return Typeface.DEFAULT;
        }
    }

    private static Typeface setTextView(Context context, String type) {
        try {
            Typeface tf = Typeface.createFromAsset(context.getAssets(), type + ".ttf");
            if (tf != null) {
                return tf;
            } else {
                return Typeface.DEFAULT;
            }
        } catch (Exception e) {
            return Typeface.DEFAULT;
        }
    }
}
