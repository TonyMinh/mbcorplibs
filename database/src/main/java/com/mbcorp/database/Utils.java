package com.mbcorp.database;

import android.database.Cursor;

/**
 * Created by Minh on 7/13/2016.
 */
public class Utils {
    public static boolean insertDB(DatabaseHelper myDB, String name, String surName, String marks) {
        return myDB.insertData(name, surName, marks);
    }

    public static String showDB(DatabaseHelper myDB) {
        StringBuffer buffer = new StringBuffer();
        Cursor res = myDB.getAllData();
        if(res.getCount() == 0) {
            buffer.append("Error! Nothing found!");
        } else {
            while (res.moveToNext()) {
                buffer.append("ID: " + res.getString(0) + "\n");
                buffer.append("Name: " + res.getString(1) + "\n");
                buffer.append("SureName: " + res.getString(2) + "\n");
                buffer.append("Marks: " + res.getString(3) + "\n");
            }
        }

        return String.valueOf(buffer);
    }

    public static boolean updateDB(DatabaseHelper myDB, String id, String name, String sureName, String marks) {
        return myDB.updateData(id, name, sureName, marks);
    }

    public static Integer deleteDB(DatabaseHelper myDB, String id) {
        return myDB.deleteData(id);
    }
}
