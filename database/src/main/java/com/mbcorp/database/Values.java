package com.mbcorp.database;

/**
 * Created by Minh on 7/13/2016.
 */
public class Values {
    public static final String DATABASE_NAME = "mbcorp.db";
    public static final String TABLE_NAME = "mbcorp_member";
    public static final String COL_1 = "ID";
    public static final String COL_2 = "NAME";
    public static final String COL_3 = "SURNAME";
    public static final String COL_4 = "MARKS";
}
