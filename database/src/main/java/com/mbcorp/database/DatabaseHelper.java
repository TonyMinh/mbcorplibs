package com.mbcorp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

/**
 * Created by Minh on 7/13/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private Context context;

    public DatabaseHelper(Context context) {
        super(context, Values.DATABASE_NAME, null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + Values.TABLE_NAME + " (" +
                Values.COL_1 + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Values.COL_2 + " TEXT, " +
                Values.COL_3 + " TEXT, " +
                Values.COL_4 + " INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Values.TABLE_NAME);
        onCreate(db);
    }

    public boolean insertData(String name, String sureName, String marks) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Values.COL_2, name);
        contentValues.put(Values.COL_3, sureName);
        contentValues.put(Values.COL_4, marks);
        long result = db.insert(Values.TABLE_NAME, null, contentValues);
        return result != -1;
    }

    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery("select * from " + Values.TABLE_NAME, null);
    }

    public boolean updateData(String id, String name, String sureName, String marks) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Values.COL_1, id);
        contentValues.put(Values.COL_2, name);
        contentValues.put(Values.COL_3, sureName);
        contentValues.put(Values.COL_4, marks);
        int check = db.update(Values.TABLE_NAME, contentValues, "id = ?", new String[] { id });
        Toast.makeText(context, "" + check, Toast.LENGTH_SHORT).show();
        return true;
    }

    public Integer deleteData(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(Values.TABLE_NAME, "ID = ?", new String[] { id });
    }
}
